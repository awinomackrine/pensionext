Ext.define('Dashboard.store.Pensioner', {
    extend: 'Ext.data.Store',
    alias: 'store.pensioners',
    model: 'Dashboard.model.Pensioner',
    
    proxy: {
        type:'rest',
        useDefaultXhrHeader:false,
        url:`http://localhost:7000/api/pensioner/202`,
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
   
    autoLoad: true
});
