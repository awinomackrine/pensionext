Ext.define('Dashboard.store.Payroll', {
    extend: 'Ext.data.Store',
    alias: 'store.payrolls',
    model: 'Dashboard.model.PayrollBatch',
      
    proxy: {
        type:'rest',
        useDefaultXhrHeader:false,
        url: 'http://localhost:7000/api/payroll/202',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalCount'
        }
    },
   
    autoLoad: true
   
});
