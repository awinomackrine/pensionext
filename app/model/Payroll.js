Ext.define('Dashboard.model.Payroll', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'id', type: 'int' },
        { name: 'month', type: 'string' },
        { name: 'year', type: 'int' },
        { name: 'gross', type: 'float' },
        { name: 'net', type: 'float' },
        { name: 'tax', type: 'float' },
        { name: 'deduction', type: 'float' },
        { name: 'arrears', type: 'float' },
        { name: 'accountNo', type: 'string' },
        { name: 'accountName', type: 'string' },
        { name: 'branchId', type: 'int' },
        { name: 'bankId', type: 'int' },
        { name: 'certifiedBy', type: 'int' },
        { name: 'certifiedDate', type: 'date' },
        { name: 'approvedBy', type: 'int' },
        { name: 'approvedDate', type: 'date' }
    ]
});
