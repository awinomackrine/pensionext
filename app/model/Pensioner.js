Ext.define('Dashboard.model.Pensioner', {
    extend: 'Ext.data.Model',
    fields: [
        'id', 'dob', 'idNo', 'otherNames', 'firstName', 'surname', 'pensionerNo',
        'dcPension', 'dbPension', 'schemeId', 'addressEmail', 'addressCellPhone',
        'addressRoad', 'addressPostalAddress', 'addressFixedPhone', 'addressTown',
        'pinNumber', 'pensionFrequency', 'pensionStatus', 'payType', 'pensionerType',
        'datePrepared', 'preparedById', 'dateCertified', 'certifiedById',
        'approvedById', 'dateApproved', 'gender'
    ]
    
});
