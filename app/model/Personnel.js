Ext.define('Dashboard.model.Personnel', {
    extend: 'Dashboard.model.Base',

    fields: [
        'name', 'email', 'phone'
    ]
});
