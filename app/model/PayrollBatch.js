Ext.define('Dashboard.model.PayrollBatch', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'id', type: 'int' },
        { name: 'month', type: 'string' },
        { name: 'year', type: 'int' },
        { name: 'batchCount', type: 'int' },
        { name: 'netTotal', type: 'float' }
    ]
});
