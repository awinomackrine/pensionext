Ext.define('Dashboard.view.pensioner.PensionerForm', {
    extend: 'Ext.form.Panel',
    xtype: 'pensionerform',
    controller: 'pensionerformcontroller',
    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },
    height: 750,
    width: 1300,
    scrollable: true,
    style: {
        padding: '20px' 
    },
    items: [{
        xtype: 'container',
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        defaults: {
            xtype: 'fieldset',
            layout: 'anchor',
            flex: 1, 
            margin: '0 10px' 
        },
        items: [{
            title: 'Personal Information',
            defaults: {
                anchor: '100%',
                xtype: 'textfield'
            },
            items: [{
                fieldLabel: 'ID',
                name: 'id',
                hidden: true
            }, {
                fieldLabel: 'Date of Birth',
                xtype: 'datefield',
                name: 'dob',
                format: 'Y-m-d',
                allowBlank: false
            }, {
                fieldLabel: 'ID Number',
                name: 'idNo'
            }, {
                fieldLabel: 'Other Names',
                name: 'otherNames'
            }, {
                fieldLabel: 'First Name',
                name: 'firstName',
                allowBlank: false
            }, {
                fieldLabel: 'Surname',
                name: 'surname',
                allowBlank: false
            }]
        }, {
            title: 'Pension Information',
            defaults: {
                anchor: '100%',
                xtype: 'textfield'
            },
            items: [{
                fieldLabel: 'Pensioner No',
                name: 'pensionerNo'
            }, {
                fieldLabel: 'DC Pension',
                xtype: 'numberfield',
                name: 'dcPension',
                minValue: 0
            }, {
                fieldLabel: 'DB Pension',
                xtype: 'numberfield',
                name: 'dbPension',
                minValue: 0
            }, {
                fieldLabel: 'Scheme ID',
                name: 'schemeId'
            }]
        }]
    }, {
        xtype: 'container',
        layout: {
            type: 'hbox',
            align: 'stretch' 
        },
        defaults: {
            xtype: 'fieldset',
            layout: 'anchor',
            flex: 1, 
            margin: '0 10px' 
        },
        items: [{
            title: 'Contact Information',
            defaults: {
                anchor: '100%',
                xtype: 'textfield'
            },
            items: [{
                fieldLabel: 'Email',
                name: 'addressEmail',
                vtype: 'email'
            }, {
                fieldLabel: 'Cell Phone',
                name: 'addressCellPhone'
            }, {
                fieldLabel: 'Road',
                name: 'addressRoad'
            }, {
                fieldLabel: 'Postal Address',
                name: 'addressPostalAddress'
            }, {
                fieldLabel: 'Town',
                name: 'addressTown'
            }, {
                fieldLabel: 'PIN Number',
                name: 'pinNumber'
            }]
        }, {
            title: 'Additional Information',
            defaults: {
                anchor: '100%',
                xtype: 'textfield'
            },
            items:[{
                fieldLabel: 'Pension Frequency',
                name: 'pensionFrequency',
                xtype: 'combobox',  
                store: ['SEMI_ANNUALY', 'ANNUALY', 'MONTHLY', 'QUARTERLY']
            }, {
                fieldLabel: 'Pension Status',
                name: 'pensionStatus',
                xtype: 'combobox',
                allowBlank: false,
                store: ['ACTIVE', 'SUSPENDED']  
            }, {
                fieldLabel: 'Pay Type',
                name: 'payType',
                xtype: 'combobox',
                store: ['BANK', ' FINANCIAL_INSTITUTION', 'PAYPOINT', 'DIRECT CHEQUE', 'MOBILE MONEY']  
            }, {
                fieldLabel: 'Pensioner Type',
                name: 'pensionerType',
                xtype: 'combobox',
                store: ['WIDOW_WIDOWER', 'ORPHAN', 'SELF', 'SUCCESSION_ESTATE', 'SUCCESSION_ESTATE', ' POTENTIAL_ANNUITANT', ' DRAWDOWN'] 
            },
             {
                fieldLabel: 'Date Approved',
                xtype: 'datefield',
                name: 'dateApproved',
                format: 'Y-m-d'
            }, {
                fieldLabel: 'Gender',
                xtype: 'combobox',
                name: 'gender',
                store: ['MALE', 'FEMALE' ]  
            }]
        }]
    }],

    buttons: [{
        text: 'Submit',
        formBind: true,
        handler: function () {
            var form = this.up('form');
            if (form.isValid()) {
                form.fireEvent('submitform', form);
            }
        },
        style: {
            marginRight: '10px'
        }
    }, {
        text: 'Cancel',
        handler: function () {
            this.up('window').close();
        }
    }]
});
