Ext.define('Dashboard.view.payroll.RunPayrollForm', {
    extend: 'Ext.form.Panel',
    xtype: 'payrollform',
    controller: 'payrollformcontroller',
    layout: 'fit',
    defaults: {
        anchor: '100%'
    },
    height: 300,
    width: 600,
    scrollable: true,
    style: {
        padding: '20px'
    },
    items: [{
        xtype: 'container',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        defaults: {
            labelWidth: 120,
            margin: '0 0 10 0',
            flex: 1
        },
        items: [{
            fieldLabel: 'Scheme ID',
            name: 'schemeId',
            xtype: 'textfield',
            allowBlank: false
        }, {
            fieldLabel: 'Month',
            name: 'month',
            xtype: 'combobox',
            allowBlank: false,
            store: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            forceSelection: true,
            queryMode: 'local'
        }, {
            fieldLabel: 'Year',
            name: 'year',
            xtype: 'numberfield',
            minValue: 0,
            allowBlank: false
        }, {
            fieldLabel: 'Date Prepared',
            name: 'datePrepared',
            xtype: 'datefield',
            format: 'Y-m-d',
            allowBlank: false
        }]
    }],
    buttons: [{
        text: 'Submit',
        formBind: true,
        handler: function () {
            var form = this.up('form');
            if (form.isValid()) {
                form.fireEvent('submitform', form);
            }
        },
        style: {
            marginRight: '10px'
        }
    }, {
        text: 'Cancel',
        handler: function () {
            this.up('window').close();
        }
    }]
});
