Ext.define('Dashboard.view.pensioner.PensionerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.pensionercontroller',

    onAddPensionerButtonClick: function(button) {
        var form = Ext.create('Dashboard.view.pensioner.PensionerForm');

        var window = Ext.create('Ext.window.Window', {
            title: 'Add Pensioner',
            layout: 'fit',
            items: form,
            modal: true,
            closable: true,
            closeAction: 'destroy'
        });

        window.show();
    },
  
    onNameSearchChange: function(field, newValue, oldValue) {
        var grid = this.getView();
        var store = grid.getStore();

        store.clearFilter(); 
        if (newValue) {
            store.filterBy(function(record) {
                return record.get('firstName').toLowerCase().indexOf(newValue.toLowerCase()) !== -1;
            });
        }
    },

    onSchemeSearchChange: function(field, newValue, oldValue) {
        var grid = this.getView();
        var store = grid.getStore();
    
        store.clearFilter(); 
        if (newValue) {
            store.filterBy(function(record) {
                return record.get('schemeId').toString().indexOf(newValue) !== -1;
            });
        }
    },

    onDateApprovedSearchChange: function(field, newValue, oldValue) {
        var grid = this.getView();
        var store = grid.getStore();
    
        store.clearFilter(); 
        if (newValue) {
            store.filterBy(function(record) {
                var dateApproved = record.get('dateApproved');
                if (Ext.isDate(dateApproved)) {
                    var formattedDate = Ext.Date.format(dateApproved, 'Y-m-d');
                    return formattedDate.indexOf(newValue) !== -1;
                }
                return false;
            });
        }
    },
    

    
});
