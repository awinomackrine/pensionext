Ext.define('Dashboard.view.pensioner.PensionerGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'pensionergrid',
    reference: 'pensionergrid',
    controller: 'pensionercontroller',
    title: 'Pensioners Grid',
    store: {
        type: 'pensioners'
    },
    selModel: {
        selType: 'checkboxmodel',
        mode: 'MULTI'
    },
    columns: [
        { text: 'ID', dataIndex: 'id', width: 50 },
        { text: 'Date of Birth', dataIndex: 'dob', flex: 1 },
        { text: 'First Name', dataIndex: 'firstName', flex: 1 },
        { text: 'Surname', dataIndex: 'surname', flex: 1 },
        { text: 'Pensioner No', dataIndex: 'pensionerNo', flex: 1 },
        { text: 'DC Pension', dataIndex: 'dcPension', flex: 1 },
        { text: 'DB Pension', dataIndex: 'dbPension', flex: 1 },
        { text: 'Scheme ID', dataIndex: 'schemeId', flex: 1 , hidden: true},
        { text: 'Email', dataIndex: 'addressEmail', flex: 1, hidden: true },
        { text: 'Cell Phone', dataIndex: 'addressCellPhone', flex: 1, hidden: true },
        { text: 'Road', dataIndex: 'addressRoad', flex: 1, hidden: true },
        { text: 'Postal Address', dataIndex: 'addressPostalAddress', flex: 1, hidden: true },
        { text: 'Fixed Phone', dataIndex: 'addressFixedPhone', flex: 1, hidden: true },
        { text: 'Town', dataIndex: 'addressTown', flex: 1, hidden: true },
        { text: 'PIN Number', dataIndex: 'pinNumber', flex: 1, hidden: true },
        { text: 'Pension Frequency', dataIndex: 'pensionFrequency', flex: 1, hidden: true },
        { text: 'Pension Status', dataIndex: 'pensionStatus', flex: 1, hidden: true },
        { text: 'Pay Type', dataIndex: 'payType', flex: 1, hidden: true },
        { text: 'Pensioner Type', dataIndex: 'pensionerType', flex: 1, hidden: true },
        { text: 'Date Prepared', dataIndex: 'datePrepared', flex: 1, hidden: true },
        { text: 'Prepared By ID', dataIndex: 'preparedById', flex: 1, hidden: true },
        { text: 'Date Certified', dataIndex: 'dateCertified', flex: 1, hidden: true },
        { text: 'Certified By ID', dataIndex: 'certifiedById', flex: 1, hidden: true },
        { text: 'Approved By ID', dataIndex: 'approvedById', flex: 1, hidden: true },
        { text: 'Date Approved', dataIndex: 'dateApproved', flex: 1, hidden: true },
        { text: 'Gender', dataIndex: 'gender', flex: 1, hidden: true },
        {
            xtype: 'actioncolumn',
            width: 80,
            items: [{
                iconCls: 'x-fa fa-eye',
                tooltip: 'View More',
                handler: function(grid, rowIndex, colIndex) {
                    var rec = grid.getStore().getAt(rowIndex);
                   
                }
            }]
        }
    ],
    tbar: [{
        text: 'Add Pensioner',
        handler: 'onAddPensionerButtonClick'
    },
  

    
    '->', {
        xtype: 'textfield',
        emptyText: 'Search by Name',
        labelAlign: 'top',
        labelWidth: 120,
        listeners: {
            change: 'onNameSearchChange'
        }
    }, {
        xtype: 'textfield',
        emptyText: 'Search by Scheme Id',
        labelAlign: 'top',
        labelWidth: 120,
        listeners: {
            change: 'onSchemeSearchChange'
        }
    },
    {
        xtype: 'datefield',
        emptyText: 'Search by Date Approved',
        labelAlign: 'top',
        labelWidth: 150,
        format: 'Y-m-d',
        listeners: {
            change: 'onDateApprovedSearchChange'
        }
    },
    
    ],
    
    height: 800,
    width: '100%',
    bbar: {
        xtype: 'pagingtoolbar',
        displayInfo: true
    },
   
});
