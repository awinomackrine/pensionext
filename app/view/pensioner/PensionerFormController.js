Ext.define('Dashboard.view.pensioner.PensionerFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.pensionerformcontroller',

    init: function() {
        this.control({
            'pensionerform': {
                submitform: 'onSubmitForm'
            }
        });
    },

    onSubmitForm: function(form) {
        var formValues = form.getForm().getValues();
        console.log('Submitted Values:', formValues);
        
        Ext.Ajax.request({
            url: 'http://localhost:7000/api/pensioner',
            method: 'POST',
            jsonData: formValues,
            success: function(response) {
                if(response.status==201 || response.status == 200 || response.status == 400){
                console.log('Form submitted successfully');
                Ext.Msg.alert('Success', 'Form submitted successfully', function() {
                    form.up('window').close();
                    Ext.getStore('pensioners').load();
                });}
            },
            failure: function(response) {
                console.error('Form submission failed');
                Ext.Msg.alert('Error', 'Form submission failed');
            }
        });
    },
});
