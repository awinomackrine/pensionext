Ext.define('Dashboard.view.payroll.RunPayrollFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.payrollformcontroller',

    init: function() {
        this.control({
            'payrollform': {
                submitform: 'onSubmitForm'
            }
        });
    },
    onSubmitForm: function(form) {
        var values = form.getValues();
        Ext.Ajax.request({
            url: 'http://localhost:7000/api/payroll/runPayroll',
            method: 'POST',
            jsonData: values,
            success: function(response, opts) {
                Ext.Msg.alert('Success', 'Payroll submitted successfully');
                form.up('window').close();
            },
            failure: function(response, opts) {
                Ext.Msg.alert('Error', 'Failed to submit payroll');
            }
        });
    }
});
