/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('Dashboard.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },
    routes: {
        'onPensionersRoute': 'onPensionersRoute',
        'onPayrollRoute': 'onPayrollRoute',
        'onHomeRoute': 'onHomeRoute',
        
    },

    onPensionersRoute: function () {
        var mainView = Ext.ComponentQuery.query('app-main')[0];
        mainView.setActiveItem(1);
    },

    onPayrollRoute: function () {
        var mainView = Ext.ComponentQuery.query('app-main')[0];
        mainView.setActiveItem(2); 
    },

    onHomeRoute: function () {
        var mainView = Ext.ComponentQuery.query('app-main')[0];
        mainView.setActiveItem(0); 
    },
    onTabChange: function (tabPanel, newTab, oldTab) {
        var itemId = newTab.getItemId();
        this.redirectTo(itemId); 
    }
    
});
