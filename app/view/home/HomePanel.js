Ext.define('Dashboard.view.home.HomePanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'homepanel',
    
    layout: 'fit',

    items: [{
        xtype: 'container',
        layout: 'fit',
        style: 'position: relative;',
        items: [
            {
                xtype: 'container',
                html: '<div style="position: absolute; top: 200; left: 300; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); z-index: 1;"></div>' + 
                      '<div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); z-index: 2; text-align: center; color: white;">' +
                      '<h1 style="font-size: 50px; font-weight: bold; margin-bottom: 20px;"></h1>' +
                      '</div>' +
                      '<img src="https://cdn.standardmedia.co.ke/images/tuesday/lylkj9bez1tilo6m6149dc68405d2.jpg" style="width: 100%; height: 100%;" />',
            }
        ]
    }]
});

