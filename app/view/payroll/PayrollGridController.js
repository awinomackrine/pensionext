Ext.define('Dashboard.view.payroll.PayrollGridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.payrollgridcontroller',

    onMonthSearchChange: function(field, newValue, oldValue) {
        var grid = this.getView();
        var store = grid.getStore();

        store.clearFilter(); 
        if (newValue) {
            store.filterBy(function(record) {
                return record.get('month').toLowerCase().indexOf(newValue.toLowerCase()) !== -1;
            });
        }
    },
    onRunPayrollButtonClick: function(button) {
        var form = Ext.create('Dashboard.view.payroll.RunPayrollForm');

        var window = Ext.create('Ext.window.Window', {
            title: 'Run Payroll',
            layout: 'fit',
            items: form,
            modal: true,
            closable: true,
            closeAction: 'destroy'
        });

        window.show();
    },

    onYearSearchChange: function(field, newValue, oldValue) {
        var grid = this.getView();
        var store = grid.getStore();
    
        store.clearFilter(); 
        if (newValue) {
            store.filterBy(function(record) {
                return record.get('year').toString().indexOf(newValue) !== -1;
            });
        }
    },
    
    onBankIdSearchChange: function(field, newValue, oldValue) {
        var grid = this.getView();
        var store = grid.getStore();
    
        store.clearFilter(); 
        if (newValue) {
            store.filterBy(function(record) {
                return record.get('bankId').toString().indexOf(newValue) !== -1;
            });
        }
    },
    

   
    onViewDetailsClick: function(button) {
        var payrollPanel = this.getView();
        var payrollDetailsGrid = payrollPanel.down('payrolldetailsgrid');
    
        if (payrollPanel.getHeight() === 800) {
            payrollPanel.setHeight(400);
            payrollDetailsGrid.setHeight(400);
            button.setText("Hide Details");
        } else {
            payrollPanel.setHeight(800);
            payrollDetailsGrid.setHeight(0);
            button.setText("Show Details");
        }
    }
    
    
});
