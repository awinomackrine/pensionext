
Ext.define('Dashboard.view.payroll.Payroll', {
    extend: 'Ext.panel.Panel',
    xtype: 'payrollssss',
    items: [{
        xtype: 'panel',
        layout: 'column',
        items: [

            {
                xtype: 'payrollgrid'
            },

            {
                xtype: 'payrolldetailsgrid'
            },
        ]
    }]

})