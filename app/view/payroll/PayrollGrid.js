Ext.define('Dashboard.view.payroll.PayrollGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'payrollgrid',
    controller: 'payrollgridcontroller',
    title: 'Payroll Grid',
    height: 800,
    store: {
        type: 'payrolls'
    },
    columns: [
        { text: 'ID', dataIndex: 'id', flex: 1 ,  hidden: true },
        { text: 'Month', dataIndex: 'month', flex: 1 },
        { text: 'Year', dataIndex: 'year', flex: 1 },
        { text: 'Batch Count', dataIndex: 'batchCount', flex: 1 },
        { text: 'Net Total', dataIndex: 'netTotal', flex: 1 }
    ],
    selModel: {
        selType: 'checkboxmodel',
        mode: 'MULTI'
    },
    tbar: [
        {
            text: 'Run Payroll',
            handler: 'onRunPayrollButtonClick'
        },
        {
            xtype: 'datefield',
            emptyText: 'Search by Month',
            labelAlign: 'top',
            labelWidth: 120,
            format: 'F', 
            reference: 'monthSearchField',
            listeners: {
                change: 'onMonthSearchChange'
            }
        },
        {
            xtype: 'numberfield',
            emptyText: 'Search by Year',
            labelAlign: 'top',
            labelWidth: 120,
            reference: 'yearSearchField',
            listeners: {
                change: 'onYearSearchChange'
            }
        },
        {
            xtype: 'textfield',
            emptyText: 'Search by Bank ID',
            labelAlign: 'top',
            labelWidth: 120,
            reference: 'bankIdSearchField',
            listeners: {
                change: 'onBankIdSearchChange'
            }
        },
        '->',
        {
            text: 'Show Details',
            handler: 'onViewDetailsClick'
        }
    ],
    bbar: {
        xtype: 'pagingtoolbar',
        displayInfo: true
    }
});
